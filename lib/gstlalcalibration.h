/*
 * Copyright (C) 2022  Aaron Viets
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


/*
 * ============================================================================
 *
 *				Preamble
 *
 * ============================================================================
 */


#ifndef __GSTLALCALIBRATION_H__
#define __GSTLALCALIBRATION_H__


#include <glib.h>
#include <gst/gst.h>


G_BEGIN_DECLS


GValue gstlal_gst_value_array_from_doubles(const gdouble *src, gint n);
gdouble *gstlal_doubles_from_gst_value_array(GValue *va, gdouble *dest, gint *n);
gsl_matrix *gstlal_gsl_matrix_from_gst_value_array(GValue *va);
GValue gstlal_gst_value_array_from_gsl_matrix(const gsl_matrix *matrix);


G_END_DECLS


#endif  /* __GSTLALCALIBRATION_H__ */
