/*
 * Copyright (C) 2022  Aaron Viets <aaron.viets@ligo.org>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef __GSTLAL_SWEEP_H__
#define __GSTLAL_SWEEP_H__


#include <complex.h>

#include <glib.h>
#include <gst/gst.h>
#include <gst/base/gstbasesink.h>


G_BEGIN_DECLS


/*
 * lal_sweep element
 */


#define GSTLAL_SWEEP_TYPE \
	(gstlal_sweep_get_type())
#define GSTLAL_SWEEP(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST((obj), GSTLAL_SWEEP_TYPE, GSTLALSweep))
#define GSTLAL_SWEEP_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST((klass), GSTLAL_SWEEP_TYPE, GSTLALSweepClass))
#define GST_IS_GSTLAL_SWEEP(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE((obj), GSTLAL_SWEEP_TYPE))
#define GST_IS_GSTLAL_SWEEP_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE((klass), GSTLAL_SWEEP_TYPE))


typedef struct _GSTLALSweep GSTLALSweep;
typedef struct _GSTLALSweepClass GSTLALSweepClass;


/**
 * GSTLALSweep:
 */


struct _GSTLALSweep {
	GstBaseSink basesink;

	/* stream info */
	gint rate;
	gint unit_size;

	/* timestamp bookkeeping */
	GstClockTime t0;
	guint64 offset0;
	guint64 next_in_offset;

	/* properties */
	double frequency;
	double *tf_freqs;
	guint64 nfreqs;
	guint64 max_nfreqs;
	complex double *transfer_function;
	double *tf_uncertainty;
	char *filename;

	/* element memory */
	complex double *numerator_data;
	complex double *denominator_data;
	guint64 ndata;
	guint64 max_ndata;
	double tzero;
};


/**
 * GSTLALSweepClass:
 * @parent_class:  the parent class
 */


struct _GSTLALSweepClass {
	GstBaseSinkClass parent_class;
};


GType gstlal_sweep_get_type(void);


G_END_DECLS


#endif	/* __GSTLAL_SWEEP_H__ */
