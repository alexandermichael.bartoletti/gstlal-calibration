#!/usr/bin/env python3
#
# Copyright (C) 2021  Aaron Viets
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#
# =============================================================================
#
#				   Preamble
#
# =============================================================================
#


import matplotlib; matplotlib.use('Agg')
import numpy as np
import matplotlib.patches as mpatches
from matplotlib import rc
rc('text', usetex = True)
matplotlib.rcParams['font.family'] = 'Times New Roman'
matplotlib.rcParams['font.size'] = 22
matplotlib.rcParams['legend.fontsize'] = 18
matplotlib.rcParams['mathtext.default'] = 'regular'
import matplotlib.pyplot as plt

from optparse import OptionParser, Option

import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
Gst.init(None)

import lal
from lal import LIGOTimeGPS

from gstlal import pipeparts
from gstlalcalibration import calibration_parts
from gstlal import simplehandler
from gstlal import datasource

from ligo import segments
from gstlal import test_common

from gstlal import FIRtools as fir
from ticks_and_grid import ticks_and_grid

parser = OptionParser()
parser.add_option("--gps-start-time", metavar = "seconds", type = int, help = "GPS time at which to start processing data")
parser.add_option("--gps-end-time", metavar = "seconds", type = int, help = "GPS time at which to stop processing data")
parser.add_option("--ifo", metavar = "name", type = str, help = "Name of the interferometer (IFO), e.g., H1, L1")
parser.add_option("--frame-cache-list", metavar = "name", type = str, help = "Comma-separated list of frame cache files that contain numerators of transfer functions")
parser.add_option("--channel-list", metavar = "list", type = str, default = None, help = "Comma-separated list of channel-names of numerators")
parser.add_option("--zeros", metavar = "list", type = str, default = None, help = "Semicolon-separated list of comma-separated lists of real and imaginary parts of zeros with which to filter the ASD.")
parser.add_option("--poles", metavar = "list", type = str, default = None, help = "Semicolon-separated list of comma-separated list of real and imaginary parts of poles with which which to filter the ASD.")
parser.add_option("--gain", type = str, default = None, help = "Gain factor(s) to apply to the ASD")
parser.add_option("--sample-rate", metavar = "Hz", type = str, default = '16384', help = "Sample rate(s) of input data.")
parser.add_option("--fft-time", metavar = "seconds", type = float, default = 4, help = "Duration in seconds of FFTs used to compute ASD")
parser.add_option("--fft-spacing", metavar = "seconds", type = float, default = 2, help = "Spacing in seconds of FFTs used to compute ASD")
parser.add_option("--window", type = str, default = 'blackman', help = "Which window function to use to window the data when taking FFTs.  Options are 'blackman', (default), 'kaiser', 'dpss', 'dolph_chebyshev', and 'hann'.")
parser.add_option("--freq-res", metavar = "Hz", type = float, default = 1.0, help = "Frequency resolution of ASD.  Only applies when using Kaiser, DPSS, and Dolph Chebyshev windows.")
parser.add_option("--frequency-min", type = float, default = 10, help = "Minimum frequency for plot")
parser.add_option("--frequency-max", type = float, default = 8192, help = "Maximum frequency for plot")
parser.add_option("--ASD-min", type = float, default = 1e-24, help = "Minimum for ASD plot")
parser.add_option("--ASD-max", type = float, default = 1e-18, help = "Maximum for ASD plot")
parser.add_option("--labels", metavar = "list", type = str, default = None, help = "Comma-separated list of labels corresponding to each ASD, to be added to plot legend.")


options, filenames = parser.parse_args()

# Get frame cache list and channel list
ifo = options.ifo
frame_cache_list = options.frame_cache_list.split(',')
channel_list = options.channel_list.split(',')
chan_list = []
for i in range(len(channel_list)):
	chan_list.append((ifo, channel_list[i]))
labels = options.labels.split(',')
if len(frame_cache_list) != len(channel_list):
	raise ValueError('Number of frame cache files must equal number of channels: %d != %d' % (len(frame_cache_list), len(channel_list)))
if len(labels) != len(channel_list):
	raise ValueError('Number of labels must equal number of channels: %d != %d' % (len(labels), len(channel_list)))

sample_rate = options.sample_rate.split(',')
sr = []
for i in range(len(labels)):
	if i < len(sample_rate):
		sr.append(int(sample_rate[i]))
	else:
		sr.append(sr[-1])

max_sr = 1
while max_sr < options.frequency_max * 2.1:
	max_sr *= 2
for i in range(len(sr)):
	sr[i] = sr[i] if sr[i] < max_sr else max_sr

# 
# =============================================================================
#
#				  Pipelines
#
# =============================================================================
#


#
# This pipeline reads in time series data and writes it to file.
#


def read_data(pipeline, name):

	for i in range(len(labels)):
		data = pipeparts.mklalcachesrc(pipeline, location = frame_cache_list[i], cache_dsc_regex = ifo)
		data = pipeparts.mkframecppchanneldemux(pipeline, data, do_file_checksum = False, skip_bad_files = True, channel_list = list(map("%s:%s".__mod__, chan_list)))
		data = calibration_parts.hook_up(pipeline, data, channel_list[i], ifo, 1.0, element_name_suffix = "%d" % i)
		data = calibration_parts.caps_and_progress(pipeline, data, "audio/x-raw,format=F64LE", labels[i])
		data = calibration_parts.mkresample(pipeline, data, 5, False, sr[i])
		pipeparts.mknxydumpsink(pipeline, data, '%s_%s_ASD_data_%d-%d.txt' % (ifo, labels[i].replace(' ', '_'), options.gps_start_time, options.gps_end_time - options.gps_start_time))

	#
	# done
	#

	return pipeline

#
# =============================================================================
#
#				     Main
#
# =============================================================================
#


# Run pipeline
test_common.build_and_run(read_data, "read_data", segment = segments.segment((LIGOTimeGPS(0, 1000000000 * options.gps_start_time), LIGOTimeGPS(0, 1000000000 * options.gps_end_time))))

# Frequency vector
fft_samples = int(options.fft_time * max(sr))
fvec = np.arange(0, max(sr) / 2.0 + max(sr) / 4.0 / (fft_samples // 2), max(sr) / 2.0 / (fft_samples // 2))

# Compute ASDs
ASDs = []
for i in range(len(labels)):
	data = np.loadtxt('%s_%s_ASD_data_%d-%d.txt' % (ifo, labels[i].replace(' ', '_'), options.gps_start_time, options.gps_end_time - options.gps_start_time))
	data = np.transpose(data)[1]
	ASDs.append(fir.asd(data, sr[i], int(options.fft_time * sr[i]), int(options.fft_spacing * sr[i]), window = options.window, freq_res = options.freq_res))

# Get any zeros and poles that we want to use to filter the ASD
if options.zeros is not None:
	zeros = []
	strzeros = options.zeros.split(';')
	while(len(strzeros) < len(labels)):
		strzeros.append(strzeros[-1])

	for i in range(len(strzeros)):
		strzeros[i] = strzeros[i].split(',')
		zeros.append([])
		for j in range(len(strzeros[i]) // 2):
			z = float(strzeros[i][2 * j]) + 1j * float(strzeros[i][2 * j + 1])
			for k in range(len(ASDs[i])):
				ASDs[i][k] = ASDs[i][k] * (1.0 + 1j * fvec[k] / z)

if options.poles is not None:
	poles = [] 
	strpoles = options.poles.split(';')
	while(len(strpoles) < len(labels)):
		strpoles.append(strpoles[-1])

	for i in range(len(strpoles)):
		strpoles[i] = strpoles[i].split(',')
		poles.append([])
		for j in range(len(strpoles[i]) // 2):
			p = float(strpoles[i][2 * j]) + 1j * float(strpoles[i][2 * j + 1])
			for k in range(len(ASDs[i])):
				ASDs[i][k] = ASDs[i][k] / (1.0 + 1j * fvec[k] / p)

if options.gain is not None:
	gain = options.gain.split(',')
	for i in range(len(gain)):
		gain[i] = float(gain[i])
	while len(gain) < len(labels):
		gain.append(gain[-1])
	for i in range(len(labels)):
		ASDs[i] *= gain[i]

# Decide a color scheme.
colors = []

# Start with defaults to use if we don't recognize what we are plotting.
default_colors = ['red', 'limegreen', 'gold', 'orange', 'aqua', 'magenta', 'blue']

# Use specific colors for known version of calibration
C02_labels = ['C02', 'c02']
C01_labels = ['C01', 'c01', 'DCS', 'dcs', 'high-latency', 'High-Latency', 'High-latency', 'HIGH-LATENCY', 'high_latency', 'High_Latency', 'High_latency', 'HIGH_LATENCY', 'high latency', 'High Latency', 'High latency', 'HIGH LATENCY', 'offline', 'Offline', 'OFFLINE']
C00_labels = ['C00', 'c00', 'GDS', 'gds', 'low-latency', 'Low-Latency', 'Low-latency', 'LOW-LATENCY', 'low_latency', 'Low_Latency', 'Low_latency', 'LOW_LATENCY', 'low latency', 'Low Latency', 'Low latency', 'LOW LATENCY', 'online', 'Online', 'ONLINE']
CALCS_labels = ['CALCS', 'calcs', 'Calcs', 'CalCS', 'CAL-CS', 'Cal-CS', 'cal-cs', 'CAL_CS', 'Cal_CS', 'cal_cs', 'front', 'FRONT', 'Front']
clean_labels = ['NOLINES', 'NO_LINES', 'no_lines', 'nolines', 'NoLines', 'CLEAN', 'Clean', 'clean', 'NoiseSub', 'NOISESUB', 'noisesub', 'NOISE_SUB', 'noise_sub']
for i in range(len(labels)):
	if any ([x for x in C02_labels if x in labels[i]]):
		if any ([x for x in clean_labels if x in labels[i]]):
			colors.append('darkgreen')
		else:
			colors.append('springgreen')
	elif any ([x for x in C01_labels if x in labels[i]]):
		if any ([x for x in clean_labels if x in labels[i]]):
			colors.append('lightcoral')
		else:
			colors.append('maroon')
	elif any ([x for x in C00_labels if x in labels[i]]):
		if any ([x for x in clean_labels if x in labels[i]]):
			colors.append('navy')
		else:
			colors.append('royalblue')
	elif any([x for x in CALCS_labels if x in labels[i]]):
		colors.append('silver')
	else:
		colors.append(default_colors[i % 7])

# Make plots
freq_scale = 'log' if options.frequency_min > 0.0 and options.frequency_max / options.frequency_min > 10 else 'linear'
ASD_scale = 'log' if options.ASD_min > 0.0 and options.ASD_max / options.ASD_min > 10 else 'linear'
plt.figure(figsize = (10, 6))
for i in range(0, len(labels)):
	plt.plot(fvec[:len(ASDs[i])], ASDs[i], colors[i % 7], linewidth = 0.75) #, label = r'$%s$' % labels[i])
	if i == 0:
		patches = [mpatches.Patch(color = colors[j], label = r'$%s$' % labels[j]) for j in range(len(labels))]
		plt.legend(handles = patches, loc = 'upper right', ncol = 1)
	#leg = plt.legend(fancybox = True, loc = "lower right")
	#leg.get_frame().set_alpha(0.8)
	if i == 0:
		#plt.title(r'${\rm %s} \ %s \ / \ %s$' % (ifo, options.name, options.denominator_name))
		plt.ylabel(r'${\rm ASD}\ \left[{\rm strain / }\sqrt{\rm Hz}\right]$')
		plt.xlabel(r'${\rm Frequency \ [Hz]}$')
	ticks_and_grid(plt.gca(), xmin = options.frequency_min, xmax = options.frequency_max, ymin = options.ASD_min, ymax = options.ASD_max, xscale = freq_scale, yscale = ASD_scale)

plt.savefig('%s_%s_ASD_%d-%d.png' % (ifo, labels[i].replace(' ', '_'), options.gps_start_time, options.gps_end_time - options.gps_start_time))
plt.savefig('%s_%s_ASD_%d-%d.pdf' % (ifo, labels[i].replace(' ', '_'), options.gps_start_time, options.gps_end_time - options.gps_start_time))


