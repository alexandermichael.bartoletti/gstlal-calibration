#!/usr/bin/env python3
# Copyright (C) 2021  Aaron Viets
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#
# =============================================================================
#
#				  Preamble
#
# =============================================================================
#


import matplotlib; matplotlib.use('Agg')
import sys
import os
import numpy as np
from matplotlib import rc
rc('text', usetex = True)
matplotlib.rcParams['font.family'] = 'Times New Roman'
matplotlib.rcParams['font.size'] = 30
matplotlib.rcParams['legend.fontsize'] = 22
matplotlib.rcParams['mathtext.default'] = 'regular'
import glob
import matplotlib.pyplot as plt
from matplotlib import ticker, cm
import matplotlib.gridspec as gridspec
from ticks_and_grid import ticks_and_grid
import time
import datetime
from optparse import OptionParser, Option
import configparser

parser = OptionParser()
parser.add_option("--frame-cache", metavar = "name", help = "Frame cache file with the data")
parser.add_option("--config-file-list", metavar = "list", help = "Comma-separated list of config files to read to determine how to access and process the data")
parser.add_option("--endtimes-list", metavar = "list", help = "Comma-separated list of GPS times at which the config files cease to be valid")

options, filenames = parser.parse_args()

config_files = options.config_file_list.split(',')
endtimes = options.endtimes_list.split(',')
for i in range(len(endtimes)):
	endtimes[i] = int(endtimes[i])

# Sanity check
if len(config_files) != len(endtimes):
	raise ValueError("endtimes-list must the the same length as config-file-list (%d != %d)" % (len(endtimes), len(config_files)))

# Start time
realtstart = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).timestamp()
f = open(options.frame_cache)
lines = f.readlines()
f.close()
tstart = float(lines[0].split(' ')[2])
dur = dursec = float(lines[-1].split(' ')[2]) - tstart
t_unit = 'seconds'
sec_per_t_unit = 1
if dur >= 100:
	t_unit = 'minutes'
	sec_per_t_unit *= 60
	dur /= 60
if dur >= 100:
	t_unit = 'hours'
	sec_per_t_unit *= 60
	dur /= 60
if dur >= 100:
	t_unit = 'days'
	sec_per_t_unit *= 24
	dur /= 24
#if dur >= 100:
#	t_unit = 'weeks'
#	sec_per_t_unit *= 7
#	dur /= 7
times = np.zeros(len(lines))
# Don't allow more than 15 days of data on one row
nrows = int(np.ceil(dursec / (15 * 24 * 60 * 60)))
matplotlib.rcParams["figure.figsize"] = (30, nrows * 10)
samples_per_row = len(lines) / nrows

# If there is more than one row, the x axis will have dates rather than time since start.
xticks = []
xticklabels = []
xticklabels0 = []
gps = int(tstart + 24 * 60 * 60 - 1)
utc = os.popen("lal_tconvert %d" % gps).read()
year = int(utc[24:28])
month = utc[4:7]
day = int(utc[8:10])
gps = int(os.popen("lal_tconvert %s %d 00:00:00 GMT %d" % (month, day, year)).read())
xticks.append((gps - tstart) / sec_per_t_unit)
xticklabels.append(' ')
xticklabels0.append(' ')
gps += 24 * 60 * 60
if month == 'Dec' and day == 31:
	gps += 1 # In case there is a leap second
	utc = os.popen("lal_tconvert %d" % gps).read()
	year = int(utc[20:24])
	month = utc[4:7]
	day = int(utc[8:10])
	gps = int(os.popen("lal_tconvert %s %d 00:00:00 GMT %d" % (month, day, year)).read())
while gps < tstart + dursec:
	xticks.append((gps - tstart) / sec_per_t_unit)
	utc = os.popen("lal_tconvert %d" % gps).read()
	year = int(utc[24:28])
	month = utc[4:7]
	day = int(utc[8:10])
	if len(xticklabels) == 1 or (month == 'Jan' and day == 1):
		xticklabels.append("%s %d, %d" % (month, day, year))
	elif day in [1, 5, 10, 15, 20, 25] and len(xticklabels) > 4:
		xticklabels.append("%s %d" % (month, day))
	else:
		xticklabels.append(' ')
	xticklabels0.append(' ')
	gps += 24 * 60 * 60
	if month == 'Dec' and day == 31:
		gps += 1 # In case there is a leap second
		utc = os.popen("lal_tconvert %d" % gps).read()
		year = int(utc[24:28])
		month = utc[4:7]
		day = int(utc[8:10])
		gps = int(os.popen("lal_tconvert %s %d 00:00:00 GMT %d" % (month, day, year)).read())

# Make a frequency vector
fvec = [10.0]
fvec_indices = [40]
while fvec[-1] < 500:
	fvec.append(round(fvec[-1] * 1.05 * 4) / 4.0)
	fvec_indices.append(int(fvec[-1] * 4))
fvec = np.asarray(fvec)
fvec_indices = np.asarray(fvec_indices, dtype = int)

# Size of data per row
MagArray = np.ones([len(lines), len(fvec)])
PhaseArray = np.zeros([len(lines), len(fvec)])
MagArrayBar = np.ones([len(lines), len(fvec)])
PhaseArrayBar = np.zeros([len(lines), len(fvec)])

# Read the config files
def ConfigSectionMap(section):
	dict1 = {}
	options = Config.options(section)
	for option in options:
		try:
			dict1[option] = Config.get(section, option)
			if dict1[option] == -1:
				DebugPrint("skip: %s" % option)
		except:
			print("exception on %s!" % option)
			dict[option] = None
	return dict1

Config = configparser.ConfigParser()

chan_prefixes = []
chan_suffixes = []
Cinvres = []
TST = []
PUM = []
UIM = []
D = []
freq1 = []
freq2 = []
freqT = []
freqP = []
freqU = []
EP1 = []
EP6 = []
EP7 = []
EP8 = []
EP9 = []
EP10 = []
EP11 = []
EP12 = []
EP13 = []
EP15 = []
EP18 = []
EP19 = []
EP20 = []
EP21 = []
EP22 = []
EP23 = []
EP24 = []
EP25 = []
EP26 = []
EP27 = []
EP28 = []
EP29 = []
EP30 = []
EP31 = []
EP32 = []
EP33 = []
EP34 = []
EP35 = []
EP36 = []
EP37 = []
EP38 = []
EP39 = []
EP40 = []
EP41 = []
EP42 = []

read_frames = True

for i in range(len(config_files)):
	Config.read(config_files[i])
	CalibrationConfigs = ConfigSectionMap("CalibrationConfigurations")
	InputConfigs = ConfigSectionMap("InputConfigurations")
	OutputConfigs = ConfigSectionMap("OutputConfigurations")

	ifo = CalibrationConfigs['ifo']

	read_frames = not (os.path.exists("%s_approxKappasErrorMagArray_%d-%d.txt" % (ifo, int(tstart), int(dursec))) and os.path.exists("%s_approxKappasErrorPhaseArray_%d-%d.txt" % (ifo, int(tstart), int(dursec))) and os.path.exists("%s_approxKappasErrorFrequency_%d-%d.txt" % (ifo, int(tstart), int(dursec))))
	if OutputConfigs["chansuffix"] != "None":
		chan_suffix = OutputConfigs["chansuffix"]
	else:
		chan_suffix = ""
	chan_prefix = OutputConfigs["chanprefix"]
	chan_prefixes.append(chan_prefix)
	chan_suffixes.append(chan_suffix)

	if read_frames:
		# Search the directory tree for files with names matching the one we want.
		filters_name = InputConfigs["filtersfilename"]
		if filters_name.count('/') > 0:
			# Then the path to the filters file was given
			filters = np.load(filters_name)
		else:
			# We need to search for the filters file
			filters_paths = []
			print("\nSearching for %s ..." % filters_name)
			# Check the user's home directory
			for dirpath, dirs, files in os.walk(os.environ['HOME']):
				if filters_name in files:
					# We prefer filters that came directly from a GDSFilters directory of the calibration SVN
					if dirpath.count("GDSFilters") > 0:
						filters_paths.insert(0, os.path.join(dirpath, filters_name))
					else:
						filters_paths.append(os.path.join(dirpath, filters_name))
			# Check if there is a checkout of the entire calibration SVN
			for dirpath, dirs, files in os.walk('/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/'):
				if filters_name in files:
					# We prefer filters that came directly from a GDSFilters directory of the calibration SVN
					if dirpath.count("GDSFilters") > 0:
						filters_paths.insert(0, os.path.join(dirpath, filters_name))
					else:
						filters_paths.append(os.path.join(dirpath, filters_name))
			if not len(filters_paths):
				raise ValueError("Cannot find filters file %s in home directory %s or in /ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/*/GDSFilters", (filters_name, os.environ['HOME']))
			print("Loading calibration filters from %s\n" % filters_paths[0])
			filters = np.load(filters_paths[0])

		Cinvres.append(np.zeros(len(fvec), dtype = np.complex128))
		TST.append(np.zeros(len(fvec), dtype = np.complex128))
		PUM.append(np.zeros(len(fvec), dtype = np.complex128))
		UIM.append(np.zeros(len(fvec), dtype = np.complex128))
		D.append(np.zeros(len(fvec), dtype = np.complex128))
		for j in range(len(fvec)):
			Cinvres[i][j] = filters['invsens_model'][1][fvec_indices[j]] + 1j * filters['invsens_model'][2][fvec_indices[j]]
			TST[i][j] = filters['tst_model'][1][fvec_indices[j]] + 1j * filters['tst_model'][2][fvec_indices[j]]
			PUM[i][j] = filters['pum_model'][1][fvec_indices[j]] + 1j * filters['pum_model'][2][fvec_indices[j]]
			UIM[i][j] = filters['uim_model'][1][fvec_indices[j]] + 1j * filters['uim_model'][2][fvec_indices[j]]
			D[i][j] = filters['D_model'][1][fvec_indices[j]] + 1j * filters['D_model'][2][fvec_indices[j]]

		# Need to remove the time-dependent portion from Cinv
		fcc = float(filters['fcc'])
		fs_squared = float(filters['fs_squared'])
		Q = filters['srcQ']
		if fs_squared < 0:
			Q = -1j * Q
		fs_over_Q = np.real(np.sqrt(complex(fs_squared)) / Q)
		Cinvres[i] = Cinvres[i] / (1.0 + 1j * fvec / fcc) * (fvec * fvec) / (fvec * fvec + fs_squared - 1j * fvec * fs_over_Q)

		freq1.append(float(filters['ka_pcal_line_freq']))
		freq2.append(float(filters['kc_pcal_line_freq']))
		freqT.append(float(filters['ktst_esd_line_freq']))
		freqP.append(float(filters['pum_act_line_freq']))
		freqU.append(float(filters['uim_act_line_freq']))

		EP1.append(float(filters['EP1_real']) + 1j * float(filters['EP1_imag']))
		EP6.append(float(filters['EP6_real']) + 1j * float(filters['EP6_imag']))
		EP7.append(float(filters['EP7_real']) + 1j * float(filters['EP7_imag']))
		EP8.append(float(filters['EP8_real']) + 1j * float(filters['EP8_imag']))
		EP9.append(float(filters['EP9_real']) + 1j * float(filters['EP9_imag']))
		EP10.append(float(filters['EP10_real']) + 1j * float(filters['EP10_imag']))
		EP11.append(float(filters['EP11_real']) + 1j * float(filters['EP11_imag']))
		EP12.append(float(filters['EP12_real']) + 1j * float(filters['EP12_imag']))
		EP13.append(float(filters['EP13_real']) + 1j * float(filters['EP13_imag']))
		EP15.append(float(filters['EP15_real']) + 1j * float(filters['EP15_imag']))
		EP18.append(float(filters['EP18_real']) + 1j * float(filters['EP18_imag']))
		EP19.append(float(filters['EP19_real']) + 1j * float(filters['EP19_imag']))
		EP20.append(float(filters['EP20_real']) + 1j * float(filters['EP20_imag']))
		EP21.append(float(filters['EP21_real']) + 1j * float(filters['EP21_imag']))
		EP22.append(float(filters['EP22_real']) + 1j * float(filters['EP22_imag']))
		EP23.append(float(filters['EP23_real']) + 1j * float(filters['EP23_imag']))
		EP24.append(float(filters['EP24_real']) + 1j * float(filters['EP24_imag']))
		EP25.append(float(filters['EP25_real']) + 1j * float(filters['EP25_imag']))
		EP26.append(float(filters['EP26_real']) + 1j * float(filters['EP26_imag']))
		EP27.append(float(filters['EP27_real']) + 1j * float(filters['EP27_imag']))
		EP28.append(float(filters['EP28_real']) + 1j * float(filters['EP28_imag']))
		EP29.append(float(filters['EP29_real']) + 1j * float(filters['EP29_imag']))
		EP30.append(float(filters['EP30_real']) + 1j * float(filters['EP30_imag']))
		EP31.append(float(filters['EP31_real']) + 1j * float(filters['EP31_imag']))
		EP32.append(float(filters['EP32_real']) + 1j * float(filters['EP32_imag']))
		EP33.append(float(filters['EP33_real']) + 1j * float(filters['EP33_imag']))
		EP34.append(float(filters['EP34_real']) + 1j * float(filters['EP34_imag']))
		EP35.append(float(filters['EP35_real']) + 1j * float(filters['EP35_imag']))
		EP36.append(float(filters['EP36_real']) + 1j * float(filters['EP36_imag']))
		EP37.append(float(filters['EP37_real']) + 1j * float(filters['EP37_imag']))
		EP38.append(float(filters['EP38_real']) + 1j * float(filters['EP38_imag']))
		EP39.append(float(filters['EP39_real']) + 1j * float(filters['EP39_imag']))
		EP40.append(float(filters['EP40_real']) + 1j * float(filters['EP40_imag']))
		EP41.append(float(filters['EP41_real']) + 1j * float(filters['EP41_imag']))
		EP42.append(float(filters['EP42_real']) + 1j * float(filters['EP42_imag']))

		if i == 0:
			oldfcc = float(filters['fcc'])
			oldfs_squared = float(filters['fs_squared'])
			oldQ = filters['srcQ']

if read_frames:
	oldkc = 1.0
	if oldfs_squared < 0:
		oldQ = -1j * oldQ
	oldfs_over_Q = np.real(np.sqrt(complex(oldfs_squared)) / oldQ)

	if os.path.exists("%s_approxKappasTimes_%d-%d.txt" % (ifo, int(tstart), int(dursec))):
		os.remove("%s_approxKappasTimes_%d-%d.txt" % (ifo, int(tstart), int(dursec)))
	if os.path.exists("%s_approxKappasTDCFs_%d-%d.txt" % (ifo, int(tstart), int(dursec))):
		os.remove("%s_approxKappasTDCFs_%d-%d.txt" % (ifo, int(tstart), int(dursec)))

config_index = 0
num_tries = 0
for i in range(len(lines)):

	# Find the right config file
	t = float(lines[i].split(' ')[2])
	compute_time = int(datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).timestamp() - realtstart)
	compute_hours = compute_time // 3600
	compute_minutes = (compute_time - 3600 * compute_hours) // 60
	compute_seconds = compute_time - 3600 * compute_hours - 60 * compute_minutes
	print('%02d:%02d:%02d   %d/%d (%0.2f%%)' % (compute_hours, compute_minutes, compute_seconds, i, len(lines), 100.0 * i / len(lines)))
	times[i] = (t - tstart) / sec_per_t_unit

	if read_frames:
		while t > endtimes[config_index]:
			config_index += 1

		# Keep frequency names simple
		f1 = freq1[config_index]
		f2 = freq2[config_index]
		fT = freqT[config_index]
		fP = freqP[config_index]
		fU = freqU[config_index]

		# Read approximate TDCFs from the frames
		# Try this first
		frdump0 = os.popen('FrDump -d 4 -f %d -l %d -i %s' % (int(t), int(t), lines[i].split('localhost')[1])).read()
		if 'Vector:%s:%sCALIB_KAPPA_TST_REAL%s' % (ifo, chan_prefixes[config_index], chan_suffixes[config_index]) in frdump0:
			dqvecmin = int(frdump0.split('Vector:%s:%sCALIB_STATE_VECTOR%s' % (ifo, chan_prefixes[config_index], chan_suffixes[config_index]))[1].split('min ')[1].split(' ')[0])
			dqvecmax = int(frdump0.split('Vector:%s:%sCALIB_STATE_VECTOR%s' % (ifo, chan_prefixes[config_index], chan_suffixes[config_index]))[1].split('max ')[1].split(' ')[0])
			if (dqvecmin % 2 == 1 and dqvecmax % 2 == 1) or i == 0:
				frdump = frdump0
		else:
			# Then try reading all the frames in the file
			frdump0 = os.popen('FrDump -d 4 -i %s' % lines[i].split('localhost')[1]).read()
			if 'Vector:%s:%sCALIB_KAPPA_TST_REAL%s' % (ifo, chan_prefixes[config_index], chan_suffixes[config_index]) in frdump0:
				dqvecmin = int(frdump0.split('Vector:%s:%sCALIB_STATE_VECTOR%s' % (ifo, chan_prefixes[config_index], chan_suffixes[config_index]))[1].split('min ')[1].split(' ')[0])
				dqvecmax = int(frdump0.split('Vector:%s:%sCALIB_STATE_VECTOR%s' % (ifo, chan_prefixes[config_index], chan_suffixes[config_index]))[1].split('max ')[1].split(' ')[0])
				if (dqvecmin % 2 == 1 and dqvecmax % 2 == 1) or i == 0:
					frdump = frdump0
			# Otherwise frdump remains what it was before

		ktstr = float(frdump.split('Vector:%s:%sCALIB_KAPPA_TST_REAL%s' % (ifo, chan_prefixes[config_index], chan_suffixes[config_index]))[1].split('mean ')[1].split(' ')[0])
		ktsti = float(frdump.split('Vector:%s:%sCALIB_KAPPA_TST_IMAGINARY%s' % (ifo, chan_prefixes[config_index], chan_suffixes[config_index]))[1].split('mean ')[1].split(' ')[0])
		kpumr = float(frdump.split('Vector:%s:%sCALIB_KAPPA_PUM_REAL%s' % (ifo, chan_prefixes[config_index], chan_suffixes[config_index]))[1].split('mean ')[1].split(' ')[0])
		kpumi = float(frdump.split('Vector:%s:%sCALIB_KAPPA_PUM_IMAGINARY%s' % (ifo, chan_prefixes[config_index], chan_suffixes[config_index]))[1].split('mean ')[1].split(' ')[0])
		kuimr = float(frdump.split('Vector:%s:%sCALIB_KAPPA_UIM_REAL%s' % (ifo, chan_prefixes[config_index], chan_suffixes[config_index]))[1].split('mean ')[1].split(' ')[0])
		kuimi = float(frdump.split('Vector:%s:%sCALIB_KAPPA_UIM_IMAGINARY%s' % (ifo, chan_prefixes[config_index], chan_suffixes[config_index]))[1].split('mean ')[1].split(' ')[0])
		kc = float(frdump.split('Vector:%s:%sCALIB_KAPPA_C%s' % (ifo, chan_prefixes[config_index], chan_suffixes[config_index]))[1].split('mean ')[1].split(' ')[0])
		fcc = float(frdump.split('Vector:%s:%sCALIB_F_CC%s' % (ifo, chan_prefixes[config_index], chan_suffixes[config_index]))[1].split('mean ')[1].split(' ')[0])
		fs_squared = float(frdump.split('Vector:%s:%sCALIB_F_S_SQUARED%s' % (ifo, chan_prefixes[config_index], chan_suffixes[config_index]))[1].split('mean ')[1].split(' ')[0])
		Qinv = float(frdump.split('Vector:%s:%sCALIB_SRC_Q_INVERSE%s' % (ifo, chan_prefixes[config_index], chan_suffixes[config_index]))[1].split('mean ')[1].split(' ')[0])

		# Calculations
		ktst = abs(ktstr + 1j * ktsti)
		kpum = abs(kpumr + 1j * kpumi)
		kuim = abs(kuimr + 1j * kuimi)
		tautst = np.angle(ktstr + 1j * ktsti) / 2 / np.pi / fT
		taupum = np.angle(kpumr + 1j * kpumi) / 2 / np.pi / fP
		tauuim = np.angle(kuimr + 1j * kuimi) / 2 / np.pi / fU
		#print("ktst=%f" % ktst)
		#print("kpum=%f" % kpum)
		#print("kuim=%f" % kuim)
		#print("tautst=%f" % tautst)
		#print("taupum=%f" % taupum)
		#print("tauuim=%f" % tauuim)
		#print("kc=%f" % kc)
		#print("fcc=%f" % fcc)
		#print("fs_squared=%f" % fs_squared)
		#print("Qinv=%f" % Qinv)
		fs = np.sqrt(complex(fs_squared))
		if fs_squared < 0:
			Qinv = 1j * Qinv

		# Calculate the X's, the injection ratios
		Sc1 = kc / (1.0 + 1j * f1 / fcc)
		Ss1 = f1 * f1 / (f1 * f1 + fs_squared - 1j * f1 * fs * Qinv)
		X1 = 1.0 / (Ss1 * Sc1 * EP11[config_index]) + EP12[config_index] * (ktst * np.exp(2 * np.pi * 1j * f1 * tautst) * EP13[config_index] + kpum * np.exp(2 * np.pi * 1j * f1 * taupum) * EP20[config_index] + kuim * np.exp(2 * np.pi * 1j * f1 * tauuim) * EP21[config_index])
		Sc2 = kc / (1.0 + 1j * f2 / fcc)
		X2 = 1.0 / (Sc2 * EP6[config_index]) + EP7[config_index] * (ktst * np.exp(2 * np.pi * 1j * f2 * tautst) * EP8[config_index] + kpum * np.exp(2 * np.pi * 1j * f2 * taupum) * EP18[config_index] + kuim * np.exp(2 * np.pi * 1j * f2 * tauuim) * EP19[config_index])
		XT = EP1[config_index] * X1 / (ktstr + 1j * ktsti)
		XP = EP15[config_index] * X1 / (kpumr + 1j * kpumi)
		XU = EP22[config_index] * X1 / (kuimr + 1j * kuimi)

		GT1 = EP25[config_index]
		GP1 = EP26[config_index]
		GU1 = EP27[config_index]
		GT2 = EP28[config_index]
		GP2 = EP29[config_index]
		GU2 = EP30[config_index]
		CATX = EP31[config_index] * XT
		GTT = EP32[config_index]
		GPT = EP33[config_index]
		GUT = EP34[config_index]
		CAPX = EP35[config_index] * XP
		GTP = EP36[config_index]
		GPP = EP37[config_index]
		GUP = EP38[config_index]
		CAUX = EP39[config_index] * XU
		GTU = EP40[config_index]
		GPU = EP41[config_index]
		GUU = EP42[config_index]	

		Y1 = EP11[config_index] * X1
		Y2 = EP6[config_index] * X2

		# Matrix elements
		M = np.zeros([6, 6])
		M[0][0] = (1.0 / (f1 * f1) - 1.0 / (fT * fT)) * np.real(GT2) + (1.0 / (fT * fT) - 1.0 / (f2 * f2)) * np.real(GT1) - (1.0 / (f1 * f1) - 1.0 / (f2 * f2)) * (np.real(GTT) - np.real(CATX))
		M[1][1] = (1.0 / (f1 * f1) - 1.0 / (fP * fP)) * np.real(GP2) + (1.0 / (fP * fP) - 1.0 / (f2 * f2)) * np.real(GP1) - (1.0 / (f1 * f1) - 1.0 / (f2 * f2)) * (np.real(GPP) - np.real(CAPX))
		M[2][2] = (1.0 / (f1 * f1) - 1.0 / (fU * fU)) * np.real(GU2) + (1.0 / (fU * fU) - 1.0 / (f2 * f2)) * np.real(GU1) - (1.0 / (f1 * f1) - 1.0 / (f2 * f2)) * (np.real(GUU) - np.real(CAUX))
		M[0][1] = (1.0 / (f1 * f1) - 1.0 / (fT * fT)) * np.real(GP2) + (1.0 / (fT * fT) - 1.0 / (f2 * f2)) * np.real(GP1) - (1.0 / (f1 * f1) - 1.0 / (f2 * f2)) * np.real(GPT)
		M[0][2] = (1.0 / (f1 * f1) - 1.0 / (fT * fT)) * np.real(GU2) + (1.0 / (fT * fT) - 1.0 / (f2 * f2)) * np.real(GU1) - (1.0 / (f1 * f1) - 1.0 / (f2 * f2)) * np.real(GUT)
		M[1][0] = (1.0 / (f1 * f1) - 1.0 / (fP * fP)) * np.real(GT2) + (1.0 / (fP * fP) - 1.0 / (f2 * f2)) * np.real(GT1) - (1.0 / (f1 * f1) - 1.0 / (f2 * f2)) * np.real(GTP)
		M[1][2] = (1.0 / (f1 * f1) - 1.0 / (fP * fP)) * np.real(GU2) + (1.0 / (fP * fP) - 1.0 / (f2 * f2)) * np.real(GU1) - (1.0 / (f1 * f1) - 1.0 / (f2 * f2)) * np.real(GUP)
		M[2][0] = (1.0 / (f1 * f1) - 1.0 / (fU * fU)) * np.real(GT2) + (1.0 / (fU * fU) - 1.0 / (f2 * f2)) * np.real(GT1) - (1.0 / (f1 * f1) - 1.0 / (f2 * f2)) * np.real(GTU)
		M[2][1] = (1.0 / (f1 * f1) - 1.0 / (fU * fU)) * np.real(GP2) + (1.0 / (fU * fU) - 1.0 / (f2 * f2)) * np.real(GP1) - (1.0 / (f1 * f1) - 1.0 / (f2 * f2)) * np.real(GPU)
		M[0][3] = -2 * np.pi * f2 * (1.0 / (f1 * f1) - 1.0 / (fT * fT)) * np.imag(GT2) - 2 * np.pi * f1 * (1.0 / (fT * fT) - 1.0 / (f2 * f2)) * np.imag(GT1) + 2 * np.pi * fT * (1.0 / (f1 * f1) - 1.0 / (f2 * f2)) * (np.imag(GTT) - np.imag(CATX))
		M[1][4] = -2 * np.pi * f2 * (1.0 / (f1 * f1) - 1.0 / (fP * fP)) * np.imag(GP2) - 2 * np.pi * f1 * (1.0 / (fP * fP) - 1.0 / (f2 * f2)) * np.imag(GP1) + 2 * np.pi * fP * (1.0 / (f1 * f1) - 1.0 / (f2 * f2)) * (np.imag(GPP) - np.imag(CAPX))
		M[2][5] = -2 * np.pi * f2 * (1.0 / (f1 * f1) - 1.0 / (fU * fU)) * np.imag(GU2) - 2 * np.pi * f1 * (1.0 / (fU * fU) - 1.0 / (f2 * f2)) * np.imag(GU1) + 2 * np.pi * fU * (1.0 / (f1 * f1) - 1.0 / (f2 * f2)) * (np.imag(GUU) - np.imag(CAUX))
		M[0][4] = -2 * np.pi * f2 * (1.0 / (f1 * f1) - 1.0 / (fT * fT)) * np.imag(GP2) - 2 * np.pi * f1 * (1.0 / (fT * fT) - 1.0 / (f2 * f2)) * np.imag(GP1) + 2 * np.pi * fT * (1.0 / (f1 * f1) - 1.0 / (f2 * f2)) * np.imag(GPT)
		M[0][5] = -2 * np.pi * f2 * (1.0 / (f1 * f1) - 1.0 / (fT * fT)) * np.imag(GU2) - 2 * np.pi * f1 * (1.0 / (fT * fT) - 1.0 / (f2 * f2)) * np.imag(GU1) + 2 * np.pi * fT * (1.0 / (f1 * f1) - 1.0 / (f2 * f2)) * np.imag(GUT)
		M[1][3] = -2 * np.pi * f2 * (1.0 / (f1 * f1) - 1.0 / (fP * fP)) * np.imag(GT2) - 2 * np.pi * f1 * (1.0 / (fP * fP) - 1.0 / (f2 * f2)) * np.imag(GT1) + 2 * np.pi * fP * (1.0 / (f1 * f1) - 1.0 / (f2 * f2)) * np.imag(GTP)
		M[1][5] = -2 * np.pi * f2 * (1.0 / (f1 * f1) - 1.0 / (fP * fP)) * np.imag(GU2) - 2 * np.pi * f1 * (1.0 / (fP * fP) - 1.0 / (f2 * f2)) * np.imag(GU1) + 2 * np.pi * fP * (1.0 / (f1 * f1) - 1.0 / (f2 * f2)) * np.imag(GUP)
		M[2][3] = -2 * np.pi * f2 * (1.0 / (f1 * f1) - 1.0 / (fU * fU)) * np.imag(GT2) - 2 * np.pi * f1 * (1.0 / (fU * fU) - 1.0 / (f2 * f2)) * np.imag(GT1) + 2 * np.pi * fU * (1.0 / (f1 * f1) - 1.0 / (f2 * f2)) * np.imag(GTU)
		M[3][0] = f2 * (f1 * f1 - fT * fT) * np.imag(GT2) + f1 * (fT * fT - f2 * f2) * np.imag(GT1) + fT * (f2 * f2 - f1 * f1) * (np.imag(GTT) - np.imag(CATX))
		M[4][1] = f2 * (f1 * f1 - fP * fP) * np.imag(GP2) + f1 * (fP * fP - f2 * f2) * np.imag(GP1) + fP * (f2 * f2 - f1 * f1) * (np.imag(GPP) - np.imag(CAPX))
		M[5][2] = f2 * (f1 * f1 - fU * fU) * np.imag(GU2) + f1 * (fU * fU - f2 * f2) * np.imag(GU1) + fU * (f2 * f2 - f1 * f1) * (np.imag(GUU) - np.imag(CAUX))
		M[3][1] = f2 * (f1 * f1 - fT * fT) * np.imag(GP2) + f1 * (fT * fT - f2 * f2) * np.imag(GP1) + fT * (f2 * f2 - f1 * f1) * np.imag(GPT)
		M[3][2] = f2 * (f1 * f1 - fT * fT) * np.imag(GU2) + f1 * (fT * fT - f2 * f2) * np.imag(GU1) + fT * (f2 * f2 - f1 * f1) * np.imag(GUT)
		M[4][0] = f2 * (f1 * f1 - fP * fP) * np.imag(GT2) + f1 * (fP * fP - f2 * f2) * np.imag(GT1) + fP * (f2 * f2 - f1 * f1) * np.imag(GTP)
		M[4][2] = f2 * (f1 * f1 - fP * fP) * np.imag(GU2) + f1 * (fP * fP - f2 * f2) * np.imag(GU1) + fP * (f2 * f2 - f1 * f1) * np.imag(GUP)
		M[5][0] = f2 * (f1 * f1 - fU * fU) * np.imag(GT2) + f1 * (fU * fU - f2 * f2) * np.imag(GT1) + fU * (f2 * f2 - f1 * f1) * np.imag(GTU)
		M[5][1] = f2 * (f1 * f1 - fU * fU) * np.imag(GP2) + f1 * (fU * fU - f2 * f2) * np.imag(GP1) + fU * (f2 * f2 - f1 * f1) * np.imag(GPU)
		M[3][3] = 2 * np.pi * f2 * f2 * (f1 * f1 - fT * fT) * np.real(GT2) + 2 * np.pi * f1 * f1 * (fT * fT - f2 * f2) * np.real(GT1) + 2 * np.pi * fT * fT * (f2 * f2 - f1 * f1) * (np.real(GTT) - np.real(CATX))
		M[4][4] = 2 * np.pi * f2 * f2 * (f1 * f1 - fP * fP) * np.real(GP2) + 2 * np.pi * f1 * f1 * (fP * fP - f2 * f2) * np.real(GP1) + 2 * np.pi * fP * fP * (f2 * f2 - f1 * f1) * (np.real(GPP) - np.real(CAPX))
		M[5][5] = 2 * np.pi * f2 * f2 * (f1 * f1 - fU * fU) * np.real(GU2) + 2 * np.pi * f1 * f1 * (fU * fU - f2 * f2) * np.real(GU1) + 2 * np.pi * fU * fU * (f2 * f2 - f1 * f1) * (np.real(GUU) - np.real(CAUX))
		M[3][4] = 2 * np.pi * f2 * f2 * (f1 * f1 - fT * fT) * np.real(GP2) + 2 * np.pi * f1 * f1 * (fT * fT - f2 * f2) * np.real(GP1) + 2 * np.pi * fT * fT * (f2 * f2 - f1 * f1) * np.real(GPT)
		M[3][5] = 2 * np.pi * f2 * f2 * (f1 * f1 - fT * fT) * np.real(GU2) + 2 * np.pi * f1 * f1 * (fT * fT - f2 * f2) * np.real(GU1) + 2 * np.pi * fT * fT * (f2 * f2 - f1 * f1) * np.real(GUT)
		M[4][3] = 2 * np.pi * f2 * f2 * (f1 * f1 - fP * fP) * np.real(GT2) + 2 * np.pi * f1 * f1 * (fP * fP - f2 * f2) * np.real(GT1) + 2 * np.pi * fP * fP * (f2 * f2 - f1 * f1) * np.real(GTP)
		M[4][5] = 2 * np.pi * f2 * f2 * (f1 * f1 - fP * fP) * np.real(GU2) + 2 * np.pi * f1 * f1 * (fP * fP - f2 * f2) * np.real(GU1) + 2 * np.pi * fP * fP * (f2 * f2 - f1 * f1) * np.real(GUP)
		M[5][3] = 2 * np.pi * f2 * f2 * (f1 * f1 - fU * fU) * np.real(GT2) + 2 * np.pi * f1 * f1 * (fU * fU - f2 * f2) * np.real(GT1) + 2 * np.pi * fU * fU * (f2 * f2 - f1 * f1) * np.real(GTU)
		M[5][4] = 2 * np.pi * f2 * f2 * (f1 * f1 - fU * fU) * np.real(GP2) + 2 * np.pi * f1 * f1 * (fU * fU - f2 * f2) * np.real(GP1) + 2 * np.pi * fU * fU * (f2 * f2 - f1 * f1) * np.real(GPU)

		# Vector elements
		V = np.zeros(6)
		V[0] = (1.0 / (f1 * f1) - 1.0 / (fT * fT)) * np.real(Y2) + (1.0 / (fT * fT) - 1.0 / (f2 * f2)) * np.real(Y1)
		V[1] = (1.0 / (f1 * f1) - 1.0 / (fP * fP)) * np.real(Y2) + (1.0 / (fP * fP) - 1.0 / (f2 * f2)) * np.real(Y1)
		V[2] = (1.0 / (f1 * f1) - 1.0 / (fU * fU)) * np.real(Y2) + (1.0 / (fU * fU) - 1.0 / (f2 * f2)) * np.real(Y1)
		V[3] = f2 * (f1 * f1 - fT * fT) * np.imag(Y2) + f1 * (fT * fT - f2 * f2) * np.imag(Y1)
		V[4] = f2 * (f1 * f1 - fP * fP) * np.imag(Y2) + f1 * (fP * fP - f2 * f2) * np.imag(Y1)
		V[5] = f2 * (f1 * f1 - fU * fU) * np.imag(Y2) + f1 * (fU * fU - f2 * f2) * np.imag(Y1)

		actTDCFs = np.linalg.solve(M, V)
		ktst1 = actTDCFs[0]
		kpum1 = actTDCFs[1]
		kuim1 = actTDCFs[2]
		tautst1 = actTDCFs[3] / ktst1
		taupum1 = actTDCFs[4] / kpum1
		tauuim1 = actTDCFs[5] / kuim1

		# Compute kappa_C
		G1 = ktst1 * GT1 * np.exp(2 * np.pi * 1j * f1 * tautst1) + kpum1 * GP1 * np.exp(2 * np.pi * 1j * f1 * taupum1) + kuim1 * GU1 * np.exp(2 * np.pi * 1j * f1 * tauuim1)
		G2 = ktst1 * GT2 * np.exp(2 * np.pi * 1j * f2 * tautst1) + kpum1 * GP2 * np.exp(2 * np.pi * 1j * f2 * taupum1) + kuim1 * GU2 * np.exp(2 * np.pi * 1j * f2 * tauuim1)
		Cinv_tdep1 = Y1 - G1
		Cinv_tdep2 = Y2 - G2
		Cinv_tdep1_real = np.real(Cinv_tdep1)
		Cinv_tdep1_imag = np.imag(Cinv_tdep1)
		Cinv_tdep2_real = np.real(Cinv_tdep2)
		Cinv_tdep2_imag = np.imag(Cinv_tdep2)

		H1 = -f1 * f1 * Cinv_tdep1_real
		H2 = -f2 * f2 * Cinv_tdep2_real
		I1 = -f1 * f1 * Cinv_tdep1_imag
		I2 = -f2 * f2 * Cinv_tdep2_imag
		Xi = (f2 * f2 * H1 - f1 * f1 * H2) / (f1 * f1 - f2 * f2)
		Zeta = I1 / f1 - I2 / f2
		a = np.float128(f2 * f2 * Xi * (H2 + Xi) * Zeta * Zeta)
		b = np.float128(f2 * (f2 * f2 - f1 * f1) * (H2 + Xi) * Zeta * I2 + pow(f2, 4) * (H2 + 2 * Xi) * Zeta * Zeta)
		c = np.float128(pow(f2, 3) * (f2 * f2 - f1 * f1) * Zeta * I2 + pow(f2 * f2 - f1 * f1, 2) * pow(H2 + Xi, 2) + pow(f2, 6) * Zeta * Zeta)
		d = np.float128(2 * f2 * f2 * pow(f2 * f2 - f1 * f1, 2) * (H2 + Xi))
		e = np.float128(pow(f2, 4) * pow(f2 * f2 - f1 * f1, 2))
		Delta0 = c * c - 3 * b * d + 12 * a * e
		Delta1 = 2 * pow(c, 3) - 9 * b * c * d + 27 * b * b * e + 27 * a * d * d - 72 * a * c * e
		p = (8 * a * c - 3 * b * b) / (8 * a * a)
		q = (pow(b, 3) - 4 * a * b * c + 8 * a * a * d) / (8 * pow(a, 3))
		Q0 = pow(0.5 * (Delta1 + pow(np.complex256(Delta1 * Delta1 - 4 * pow(Delta0, 3)), 0.5)), 1.0 / 3.0)
		S0 = 0.5 * pow(np.complex256(-2 * p) / 3.0 + (Q0 + Delta0 / Q0) / (3 * a), 0.5)

		#print('Delta1=%e' % Delta1)
		#print('Delta0=%e' % Delta0)
		#print('Q0=%e' % Q0)
		#print('a=%e' % a)
		#print('b=%e' % b)
		#print('c=%e' % c)
		#print('d=%e' % d)
		#print('e=%e' % e)
		#print('S0=%e' % S0)
		#print('p=%e' % p)
		#print('q=%e' % q)
		complex_kc1 = -b / (4 * a) - S0 + 0.5 * pow(-4 * S0 * S0 - 2 * p + q / S0, 0.5)
		complex_kc2 = -b / (4 * a) - S0 - 0.5 * pow(-4 * S0 * S0 - 2 * p + q / S0, 0.5)
		complex_kc3 = -b / (4 * a) + S0 + 0.5 * pow(-4 * S0 * S0 - 2 * p - q / S0, 0.5)
		complex_kc4 = -b / (4 * a) + S0 - 0.5 * pow(-4 * S0 * S0 - 2 * p - q / S0, 0.5)

		# Any solution that is clearly complex or negative is not the solution we want
		kc1 = kc2 = kc3 = kc4 = 0.0
		if(np.real(complex_kc1) > 0.0 and abs(np.imag(complex_kc1) / np.real(complex_kc1)) < 1e-3):
			kc1 = float(np.real(complex_kc1))
		if(np.real(complex_kc2) > 0.0 and abs(np.imag(complex_kc2) / np.real(complex_kc2)) < 1e-3):
			kc2 = float(np.real(complex_kc2))
		if(np.real(complex_kc3) > 0.0 and abs(np.imag(complex_kc3) / np.real(complex_kc3)) < 1e-3):
			kc3 = float(np.real(complex_kc3))
		if(np.real(complex_kc4) > 0.0 and abs(np.imag(complex_kc4) / np.real(complex_kc4)) < 1e-3):
			kc4 = float(np.real(complex_kc4))
		#print("kc1=%f+%fi" % (np.real(complex_kc1), np.imag(complex_kc1)))
		#print("kc2=%f+%fi" % (np.real(complex_kc2), np.imag(complex_kc2)))
		#print("kc3=%f+%fi" % (np.real(complex_kc3), np.imag(complex_kc3)))
		#print("kc4=%f+%fi" % (np.real(complex_kc4), np.imag(complex_kc4)))

		# Compute f_cc
		fcc1 = fcc2 = fcc3 = fcc4 = fs_squared1 = fs_squared2 = fs_squared3 = fs_squared4 = fs_over_Q1 = fs_over_Q2 = fs_over_Q3 = fs_over_Q4 = 0.0
		if(kc1 > 0.0):
			fcc1 = (f2 * f2 - f1 * f1) / (kc1 * (f2 * Cinv_tdep2_imag - f1 * Cinv_tdep1_imag))
			fs_squared1 = kc1 * (Cinv_tdep2_real - Cinv_tdep1_real) / (1.0 / (f2 * f2) - 1.0 / (f1 * f1))
			fs_over_Q1 = fcc1 * (kc1 * Cinv_tdep1_real - 1.0 - fs_squared1 / (f1 * f1))
		if(kc2 > 0.0):
			fcc2 = (f2 * f2 - f1 * f1) / (kc2 * (f2 * Cinv_tdep2_imag - f1 * Cinv_tdep1_imag))
			fs_squared2 = kc2 * (Cinv_tdep2_real - Cinv_tdep1_real) / (1.0 / (f2 * f2) - 1.0 / (f1 * f1))
			fs_over_Q2 = fcc2 * (kc2 * Cinv_tdep1_real - 1.0 - fs_squared2 / (f1 * f1))
		if(kc3 > 0.0):
			fcc3 = (f2 * f2 - f1 * f1) / (kc3 * (f2 * Cinv_tdep2_imag - f1 * Cinv_tdep1_imag))
			fs_squared3 = kc3 * (Cinv_tdep2_real - Cinv_tdep1_real) / (1.0 / (f2 * f2) - 1.0 / (f1 * f1))
			fs_over_Q3 = fcc3 * (kc3 * Cinv_tdep1_real - 1.0 - fs_squared3 / (f1 * f1))
		if(kc4 > 0.0):
			fcc4 = (f2 * f2 - f1 * f1) / (kc4 * (f2 * Cinv_tdep2_imag - f1 * Cinv_tdep1_imag))
			fs_squared4 = kc4 * (Cinv_tdep2_real - Cinv_tdep1_real) / (1.0 / (f2 * f2) - 1.0 / (f1 * f1))
			fs_over_Q4 = fcc4 * (kc4 * Cinv_tdep1_real - 1.0 - fs_squared4 / (f1 * f1))

		lowest_error = error1 = error2 = error3 = error4 = 2.0
		best_solution = 0
		if(kc1 > 0):
			fs1 = pow(complex(fs_squared1), 0.5)
			Qinv1 = fs_over_Q1 / fs1
			if(fcc1 > abs(fs1 / 2 * (Qinv1 + pow(pow(Qinv1, 2) + 4, 0.5))) and fcc1 > abs(fs1 / 2 * (Qinv1 - pow(pow(Qinv1, 2) + 4, 0.5)))):
				error1 = pow(abs(((1 + 1j * f1 / fcc1) / kc1) * ((f1 * f1 + fs_squared1 - 1j * f1 * fs_over_Q1) / (f1 * f1)) / Cinv_tdep1 - 1), 2)
				error1 += pow(abs(((1 + 1j * f2 / fcc1) / kc1) * ((f2 * f2 + fs_squared1 - 1j * f2 * fs_over_Q1) / (f2 * f2)) / Cinv_tdep2 - 1), 2)
				if(error1 < lowest_error):
					lowest_error = error1
					best_solution = 1
		if(kc2 > 0):
			fs2 = pow(complex(fs_squared2), 0.5)
			Qinv2 = fs_over_Q2 / fs2
			if(fcc2 > abs(fs2 / 2 * (Qinv2 + pow(pow(Qinv2, 2) + 4, 0.5))) and fcc2 > abs(fs2 / 2 * (Qinv2 - pow(pow(Qinv2, 2) + 4, 0.5)))):
				error2 = pow(abs(((1 + 1j * f1 / fcc2) / kc2) * ((f1 * f1 + fs_squared2 - 1j * f1 * fs_over_Q2) / (f1 * f1)) / Cinv_tdep1 - 1), 2)
				error2 += pow(abs(((1 + 1j * f2 / fcc2) / kc2) * ((f2 * f2 + fs_squared2 - 1j * f2 * fs_over_Q2) / (f2 * f2)) / Cinv_tdep2 - 1), 2)
				if(error2 < lowest_error):
					lowest_error = error2
					best_solution = 2
		if(kc3 > 0):
			fs3 = pow(complex(fs_squared3), 0.5)
			Qinv3 = fs_over_Q3 / fs3
			if(fcc3 > abs(fs3 / 2 * (Qinv3 + pow(pow(Qinv3, 2) + 4, 0.5))) and fcc3 > abs(fs3 / 2 * (Qinv3 - pow(pow(Qinv3, 2) + 4, 0.5)))):
				error3 = pow(abs(((1 + 1j * f1 / fcc3) / kc3) * ((f1 * f1 + fs_squared3 - 1j * f1 * fs_over_Q3) / (f1 * f1)) / Cinv_tdep1 - 1), 2)
				error3 += pow(abs(((1 + 1j * f2 / fcc3) / kc3) * ((f2 * f2 + fs_squared3 - 1j * f2 * fs_over_Q3) / (f2 * f2)) / Cinv_tdep2 - 1), 2)
				if(error3 < lowest_error):
					lowest_error = error3
					best_solution = 3
		if(kc4 > 0):
			fs4 = pow(complex(fs_squared4), 0.5)
			Qinv4 = fs_over_Q4 / fs4
			if(fcc4 > abs(fs4 / 2 * (Qinv4 + pow(pow(Qinv4, 2) + 4, 0.5))) and fcc4 > abs(fs4 / 2 * (Qinv4 - pow(pow(Qinv4, 2) + 4, 0.5)))):
				error4 = pow(abs(((1 + 1j * f1 / fcc4) / kc4) * ((f1 * f1 + fs_squared4 - 1j * f1 * fs_over_Q4) / (f1 * f1)) / Cinv_tdep1 - 1), 2)
				error4 += pow(abs(((1 + 1j * f2 / fcc4) / kc4) * ((f2 * f2 + fs_squared4 - 1j * f2 * fs_over_Q4) / (f2 * f2)) / Cinv_tdep2 - 1), 2)
				if(error4 < lowest_error):
					lowest_error = error4
					best_solution = 4

		if best_solution == 1:
			num_tries = 0
			kc0 = kc1
			fcc0 = fcc1
			fs_squared0 = fs_squared1
			fs_over_Q0 = fs_over_Q1
		elif best_solution == 2:
			num_tries = 0
			kc0 = kc2
			fcc0 = fcc2
			fs_squared0 = fs_squared2
			fs_over_Q0 = fs_over_Q2
		elif best_solution == 3:
			num_tries = 0
			kc0 = kc3
			fcc0 = fcc3
			fs_squared0 = fs_squared3
			fs_over_Q0 = fs_over_Q3
		elif best_solution == 4:
			num_tries = 0
			kc0 = kc4
			fcc0 = fcc4
			fs_squared0 = fs_squared4
			fs_over_Q0 = fs_over_Q4
		else:
			num_tries += 1
			if num_tries > 64:
				kc0 = kc
				fcc0 = fcc
				fs_squared0 = fs_squared
				fs_over_Q0 = np.real(pow(fs_squared, 0.5) * Qinv)
			else:
				kc0 = oldkc
				fcc0 = oldfcc
				fs_squared0 = oldfs_squared
				fs_over_Q0 = oldfs_over_Q

		#print("ktst1=%f" % ktst1)
		#print("kpum1=%f" % kpum1)
		#print("kuim1=%f" % kuim1)
		#print("tautst1=%f" % tautst1)
		#print("taupum1=%f" % taupum1)
		#print("tauuim1=%f" % tauuim1)
		#print("kc0=%f" % kc0)
		#print("fcc0=%f" % fcc0)
		#print("fs_squared0=%f" % fs_squared0)
		#print("fs_over_Q0=%f" % fs_over_Q0)
		fs = np.sqrt(complex(fs_squared))

		TimesFile = open("%s_approxKappasTimes_%d-%d.txt" % (ifo, int(tstart), int(dursec)), "a")
		TimesFile.write('%d\n' % int(t))
		TDCF_file = open("%s_approxKappasTDCFs_%d-%d.txt" % (ifo, int(tstart), int(dursec)), "a")
		TDCF_file.write('%d\tApproximate\tExact (%d)\tSolution 1\tSolution 2\tSolution 3\tSolution 4\n' % (int(t), best_solution))
		TDCF_file.write('ktst\t\t%f\t%f\n' % (ktst, ktst1))
		TDCF_file.write('tautst\t\t%e\t%e\n' % (tautst, tautst1))
		TDCF_file.write('kpum\t\t%f\t%f\n' % (kpum, kpum1))
		TDCF_file.write('taupum\t\t%e\t%e\n' % (taupum, taupum1))
		TDCF_file.write('kuim\t\t%f\t%f\n' % (kuim, kuim1))
		TDCF_file.write('tauuim\t\t%e\t%e\n' % (tauuim, tauuim1))
		TDCF_file.write('kc\t\t%f\t%f\t%e\t%e\t%e\t%e\n' % (kc, kc0, kc1, kc2, kc3, kc4))
		TDCF_file.write('fcc\t\t%f\t%f\t%e\t%e\t%e\t%e\n' % (fcc, fcc0, fcc1, fcc2, fcc3, fcc4))
		TDCF_file.write('fs_squared\t%f\t%f\t%e\t%e\t%e\t%e\n' % (fs_squared, fs_squared0, fs_squared1, fs_squared2, fs_squared3, fs_squared4))
		TDCF_file.write('fs_over_Q\t%f\t%f\t%e\t%e\t%e\t%e\n' % (np.real(fs * Qinv), fs_over_Q0, fs_over_Q1, fs_over_Q2, fs_over_Q3, fs_over_Q4))
		TDCF_file.write('uncertainty\t\t\t%e\t%e\t%e\t%e\t%e\n\n' % (lowest_error, error1, error2, error3, error4))
		TimesFile.close()
		TDCF_file.close()

		oldkc = kc0
		oldfcc = fcc0
		oldfs_squared = fs_squared0
		oldfs_over_Q = fs_over_Q0

		# Finally, compare solutions at frequencies in fvec
		ApproxR = Cinvres[config_index] * ((1.0 + 1j * fvec / fcc) / kc) * ((fvec * fvec + fs_squared - 1j * fvec * fs * Qinv) / (fvec * fvec)) + D[config_index] * (ktst * TST[config_index] * np.exp(2 * np.pi * 1j * fvec * tautst) + kpum * PUM[config_index] * np.exp(2 * np.pi * 1j * fvec * taupum) + kuim * UIM[config_index] * np.exp(2 * np.pi * 1j * fvec * tauuim))
		ExactR = Cinvres[config_index] * ((1.0 + 1j * fvec / fcc0) / kc0) * ((fvec * fvec + fs_squared0 - 1j * fvec * fs_over_Q0) / (fvec * fvec)) + D[config_index] * (ktst1 * TST[config_index] * np.exp(2 * np.pi * 1j * fvec * tautst1) + kpum1 * PUM[config_index] * np.exp(2 * np.pi * 1j * fvec * taupum1) + kuim1 * UIM[config_index] * np.exp(2 * np.pi * 1j * fvec * tauuim1))
		Error = ApproxR / ExactR

		MagArray[i] = 100 * (abs(Error) - 1)
		PhaseArray[i] = np.angle(Error) * 180 / np.pi
	MagArrayBar[i] = np.arange(-10, 10.00000001, 20 / (len(fvec) - 1))
	PhaseArrayBar[i] = np.arange(-6, 6.00000001, 12 / (len(fvec) - 1))

if read_frames:
	# Save data
	np.savetxt("%s_approxKappasErrorMagArray_%d-%d.txt" % (ifo, int(tstart), int(dursec)), MagArray)
	np.savetxt("%s_approxKappasErrorPhaseArray_%d-%d.txt" % (ifo, int(tstart), int(dursec)), PhaseArray)
else:
	# We need to get the data
	MagArray = np.loadtxt("%s_approxKappasErrorMagArray_%d-%d.txt" % (ifo, int(tstart), int(dursec)))
	PhaseArray = np.loadtxt("%s_approxKappasErrorPhaseArray_%d-%d.txt" % (ifo, int(tstart), int(dursec)))

np.savetxt("%s_approxKappasErrorFrequency_%d-%d.txt" % (ifo, int(tstart), int(dursec)), fvec)

# Make plots
realtstart = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).timestamp()
fig = plt.figure()
gs = gridspec.GridSpec(nrows, 1, hspace = 0.15)
#fig, ax = plt.subplots(2 * nrows, 1)
for i in range(nrows):

	gs0 = gridspec.GridSpecFromSubplotSpec(2, 1, subplot_spec = gs[i], hspace = 0.1)
	magax = fig.add_subplot(gs0[0])
	phaseax = fig.add_subplot(gs0[1])

	compute_time = int(datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).timestamp() - realtstart)
	compute_hours = compute_time // 3600
	compute_minutes = (compute_time - 3600 * compute_hours) // 60
	compute_seconds = compute_time - 3600 * compute_hours - 60 * compute_minutes
	print('%02d:%02d:%02d   %d/%d (%0.2f%%)' % (compute_hours, compute_minutes, compute_seconds, i, nrows, 100.0 * i / nrows))

	minidx = int(round(i * samples_per_row))
	maxidx = int(round((i + 1) * samples_per_row))

	xtick_start = len(xticks) - 1
	xtick_final = 0
	for j in range(len(xticks)):
		if times[minidx] <= xticks[-1 - j]:
			xtick_start = len(xticks) - 1 - j
		if i == nrows - 1:
			if times[maxidx - 1] >= xticks[j]:
				xtick_final = j + 1
		else:
			if times[maxidx] > xticks[j]:
				xtick_final = j + 1

	X, Y = np.meshgrid(times[minidx:maxidx], fvec)
	Z = np.transpose(MagArray[minidx:maxidx])
	line_levels = [-15,-10,-5,-2,-1,1,2,5,10,15]
	line_colors = ['lightgray', 'lightgray', 'lightgray', 'dimgray', 'dimgray', 'dimgray', 'dimgray', 'lightgray', 'lightgray', 'lightgray']
	CS = magax.contourf(X, Y, Z, 50, origin='lower', vmin=-10, vmax=10, levels=None, cmap=cm.seismic)
	con = magax.contour(CS, origin='lower', levels=line_levels, colors=line_colors, linewidths=0.5)
	if nrows > 1:
		magax.set_xticks(xticks[xtick_start:xtick_final])
		magax.set_xticklabels(xticklabels0[xtick_start:xtick_final])
	magax.clabel(con, inline=0.5, fontsize=18, fmt='%.0f')
	cbar = plt.colorbar(CS, ax = magax, ticks=[-10,-5,0,5,10])
	cbar.ax.set_ylabel('Magnitude (\%)')
	magax.set_ylabel('Frequency (Hz)')
	magax.set_yscale('log')

	Z = np.transpose(PhaseArray[minidx:maxidx])
	line_levels = [-15,-10,-5,-2,-1,1,2,5,10,15]
	line_colors = ['lightgray', 'lightgray', 'lightgray', 'dimgray', 'dimgray', 'dimgray', 'dimgray', 'lightgray', 'lightgray', 'lightgray']
	CS = phaseax.contourf(X, Y, Z, 50, origin='lower', vmin=-6, vmax=6, levels=None, cmap=cm.seismic)
	con = phaseax.contour(CS, origin='lower', levels=line_levels, colors=line_colors, linewidths=0.5)
	if nrows > 1:
		phaseax.set_xticks(xticks[xtick_start:xtick_final])
		phaseax.set_xticklabels(xticklabels[xtick_start:xtick_final])
	else:
		phaseax.set_xlabel(r'${\rm Time \  in \  %s \  since \  %s \  UTC}$' % (t_unit, time.strftime("%b %d %Y %H:%M:%S".replace(':', '{:}').replace('-', '\mbox{-}').replace(' ', '\ '), time.gmtime(tstart + 315964782))))
	phaseax.clabel(con, inline=0.5, fontsize=18, fmt='%.0f')
	cbar = plt.colorbar(CS, ax = phaseax, ticks=[-6,-4,-2,0,2,4,6], extend = 'both')
	cbar.ax.set_ylabel('Phase (deg)')
	phaseax.set_ylabel('Frequency (Hz)')
	phaseax.set_yscale('log')

plt.savefig("%s_approxKappasErrorPlot_%d-%d.pdf" % (ifo, int(tstart), int(dursec)))
plt.close()

fig = plt.figure()
gs = gridspec.GridSpec(nrows, 1, hspace = 0.15)
for i in range(nrows):

	gs0 = gridspec.GridSpecFromSubplotSpec(2, 1, subplot_spec = gs[i], hspace = 0.1)
	magax = fig.add_subplot(gs0[0])
	phaseax = fig.add_subplot(gs0[1])

	compute_time = int(datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).timestamp() - realtstart)
	compute_hours = compute_time // 3600
	compute_minutes = (compute_time - 3600 * compute_hours) // 60
	compute_seconds = compute_time - 3600 * compute_hours - 60 * compute_minutes
	print('%02d:%02d:%02d   %d/%d (%0.2f%%)' % (compute_hours, compute_minutes, compute_seconds, i, nrows, 100.0 * i / nrows))

	minidx = int(round(i * samples_per_row))
	maxidx = int(round((i + 1) * samples_per_row))

	xtick_start = len(xticks) - 1
	xtick_final = 0
	for j in range(len(xticks)):
		if times[minidx] <= xticks[-1 - j]:
			xtick_start = len(xticks) - 1 - j
		if i == nrows - 1:
			if times[maxidx - 1] >= xticks[j]:
				xtick_final = j + 1
		else:
			if times[maxidx] > xticks[j]:
				xtick_final = j + 1

	X, Y = np.meshgrid(times[minidx:maxidx], fvec)
	Z = np.transpose(MagArrayBar[minidx:maxidx])
	line_levels = [-15,-10,-5,-2,-1,1,2,5,10,15]
	line_colors = ['lightgray', 'lightgray', 'lightgray', 'dimgray', 'dimgray', 'dimgray', 'dimgray', 'lightgray', 'lightgray', 'lightgray']
	CS = magax.contourf(X, Y, Z, 50, origin='lower', vmin=-10, vmax=10, levels=None, cmap=cm.seismic)
	con = magax.contour(CS, origin='lower', levels=line_levels, colors=line_colors, linewidths=0.5)
	if nrows > 1:
		magax.set_xticks(xticks[xtick_start:xtick_final])
		magax.set_xticklabels(xticklabels0[xtick_start:xtick_final])
	magax.clabel(con, inline=0.5, fontsize=18, fmt='%.0f')
	cbar = plt.colorbar(CS, ax = magax, ticks=[-10,-5,0,5,10])
	cbar.ax.set_ylabel('Magnitude (\%)')
	magax.set_ylabel('Frequency (Hz)')
	magax.set_yscale('log')

	Z = np.transpose(PhaseArrayBar[minidx:maxidx])
	line_levels = [-15,-10,-5,-2,-1,1,2,5,10,15]
	line_colors = ['lightgray', 'lightgray', 'lightgray', 'dimgray', 'dimgray', 'dimgray', 'dimgray', 'lightgray', 'lightgray', 'lightgray']

	CS = phaseax.contourf(X, Y, Z, 50, origin='lower', vmin=-6, vmax=6, levels=None, cmap=cm.seismic)
	con = phaseax.contour(CS, origin='lower', levels=line_levels, colors=line_colors, linewidths=0.5)
	if nrows > 1:
		phaseax.set_xticks(xticks[xtick_start:xtick_final])
		phaseax.set_xticklabels(xticklabels[xtick_start:xtick_final])
	else:
		phaseax.set_xlabel(r'${\rm Time \  in \  %s \  since \  %s \  UTC}$' % (t_unit, time.strftime("%b %d %Y %H:%M:%S".replace(':', '{:}').replace('-', '\mbox{-}').replace(' ', '\ '), time.gmtime(tstart + 315964782))))
	phaseax.clabel(con, inline=0.5, fontsize=18, fmt='%.0f')
	cbar = plt.colorbar(CS, ax = phaseax, ticks=[-6,-4,-2,0,2,4,6], extend = 'both')
	cbar.ax.set_ylabel('Phase (deg)')
	phaseax.set_ylabel('Frequency (Hz)')
	phaseax.set_yscale('log')

plt.savefig("%s_approxKappasErrorPlotBar_%d-%d.pdf" % (ifo, int(tstart), int(dursec)))
plt.close()

os.system("convert %s_approxKappasErrorPlot_%d-%d.pdf -crop 1650x100000+0+0 %s_approxKappasErrorPlot_%d-%d.pdf" % (ifo, int(tstart), int(dursec), ifo, int(tstart), int(dursec)))
os.system("convert %s_approxKappasErrorPlotBar_%d-%d.pdf -crop 1650x100000+1650+0 %s_approxKappasErrorPlotBar_%d-%d.pdf" % (ifo, int(tstart), int(dursec), ifo, int(tstart), int(dursec)))
os.system("convert %s_approxKappasErrorPlot_%d-%d.pdf %s_approxKappasErrorPlotBar_%d-%d.pdf -gravity center -composite %s_approxKappasErrorPlot_%d-%d.pdf" % (ifo, int(tstart), int(dursec), ifo, int(tstart), int(dursec), ifo, int(tstart), int(dursec)))
os.system("rm %s_approxKappasErrorPlotBar_%d-%d.pdf" % (ifo, int(tstart), int(dursec)))


