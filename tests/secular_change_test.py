#!/usr/bin/env python3
# Copyright (C) 2022  Aaron Viets
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#
# =============================================================================
#
#				   Preamble
#
# =============================================================================
#


import matplotlib; matplotlib.use('Agg')
import sys
import os
import numpy as np
import time
from math import pi
import resource
import datetime
import time
import matplotlib.patches as mpatches
from matplotlib import rc
rc('text', usetex = True)
matplotlib.rcParams['font.family'] = 'Times New Roman'
matplotlib.rcParams['font.size'] = 22
matplotlib.rcParams['legend.fontsize'] = 18
matplotlib.rcParams['mathtext.default'] = 'regular'
import glob
import matplotlib.pyplot as plt

from gstlal import pipeparts
from gstlalcalibration import calibration_parts
from gstlal import test_common
from gi.repository import Gst

from ticks_and_grid import ticks_and_grid


#
# =============================================================================
#
#				  Pipelines
#
# =============================================================================
#

def secular_change_01(pipeline, name):

	#
	# This test reads kappa_tst from frames and attempts to detect secular changes
	#

	rate_in = 2048		# Hz
	buffer_length = 1.0	# seconds
	test_duration = 50.0	# seconds

	#
	# build pipeline
	#

	src = pipeparts.mklalcachesrc(pipeline, location = "H1_C00_frames.cache", cache_dsc_regex = "H1", use_mmap = True)
	src = pipeparts.mkframecppchanneldemux(pipeline, src)
	ktst = calibration_parts.hook_up(pipeline, src, "GDS-CALIB_KAPPA_TST_REAL", "H1", 1.0)
	ktst = calibration_parts.caps_and_progress(pipeline, ktst, "audio/x-raw,format=F64LE,rate=16,channels=1,bitmap=0x0", "ktst")
	ktst = calibration_parts.mkresample(pipeline, ktst, 0, False, 1)
	ktst = pipeparts.mktee(pipeline, ktst)
	pipeparts.mknxydumpsink(pipeline, pipeparts.mkgeneric(pipeline, ktst, "lal_insertgap", insert_gap = False, chop_length = int(1000000000 * 60 * 60 * 2.2)), "ktst.txt")
	# Use a 2-hour variance divided by a 2-hour mean of a 10-minute variance as an indicator of secular change.
	ratio = calibration_parts.detect_secular_change(pipeline, ktst, 2 * 60 * 60, rate = 1)
	# First 2 hours and 10 minutes of data is junk
	ratio = pipeparts.mkgeneric(pipeline, ratio, "lal_insertgap", insert_gap = False, chop_length = int(1000000000 * 60 * 60 * 2.2))
	pipeparts.mknxydumpsink(pipeline, ratio, "secular_change.txt")

	#
	# done
	#
	
	return pipeline


#
# =============================================================================
#
#				     Main
#
# =============================================================================
#


test_common.build_and_run(secular_change_01, "secular_change_01")

ktst = np.transpose(np.loadtxt("ktst.txt"))
t = ktst[0]
ktst = ktst[1]
secular_change = np.transpose(np.loadtxt("secular_change.txt"))[1]
plt.figure(figsize = (10, 10))
plt.subplot(211)
plt.plot(t, ktst, "blue", linewidth = 0.75)
ticks_and_grid(plt.gca(), ymin = 0.95, ymax = 1.05, xscale = 'linear', yscale = 'linear')
plt.ylabel("kappa TST")
plt.subplot(212)
plt.plot(t, secular_change, "blue", linewidth = 0.75)
ticks_and_grid(plt.gca(), ymin = 0, ymax = 5, xscale = 'linear', yscale = 'linear')
plt.ylabel("Secular Change")
plt.xlabel("GPS Time (s)")

plt.savefig("secular_change.png")


